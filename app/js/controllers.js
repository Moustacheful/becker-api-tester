'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('TesterCtrl', ['$scope','Facebook','$http', function($scope,Facebook,$http) {
  	$scope.login = function(){
  		Facebook.login().then(function(data){
  			console.log("okayokayokay!");
  		});
  	}
  	$scope.logout = function() {
  		Facebook.logout().then(function(data){
  			console.log(data);
  		});
  	}
  	$scope.startGame = function(){
   		$http.post("/becker-api/game/start").success(function(data){
    			$scope.result = data;

   		}).error(function(data){
    			$scope.result = data;
   			
   		})
  	}

    $scope.activateAttempt = function(attempt){
      $http.post('/becker-api/game/activate/'+attempt.type).success(function(data){
        attempt.active = true;
      })
    }

  	$scope.types = ['normal','share_fb','share_tw','referral'];
  	$scope.type = $scope.types[0];
  	$scope.guess = "10,30";
  	$scope.checkGame = function(){
 		$http.post("/becker-api/game/check/"+$scope.type+"/"+$scope.guess).success(function(data){
  			$scope.result = data;

 		}).error(function(data){
  			$scope.result = data;
 			
 		})
  	}
  }])
