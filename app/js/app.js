'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'ngSanitize',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'Facebook'
]).
config(['$routeProvider','FacebookProvider', function($routeProvider,FacebookProvider) {
	FacebookProvider.config(
		{
			appId: '1430811337171810',
			status: true, // check login status
			cookie: true, // enable cookies to allow the server to access the session
			xfbml: false // parse XFBML
		},
		"email,user_birthday"
	);
}]).
run(['Facebook','$http','$rootScope',function(Facebook,$http,$rootScope){
	$rootScope.referral = {
		token : undefined
	}

	Facebook.onLogin = 	function(response,deferred){
		console.log("onLogin")
		$http.post("/becker-api/login/"+response.authResponse.accessToken,undefined,{
			params: {
				referral_token : $rootScope.referral.token
			}
		}).success(function(userData){
			$rootScope.user = userData;
			deferred.resolve(response)
		}).error(function(data){
			deferred.reject(data.message)
		});
	};
	Facebook.onLogout = function(response,deferred){
		console.log("onLogout")
		$http.get("/becker-api/logout").success(function(data){
			deferred.resolve("okay!");
			$rootScope.user = undefined;
		})
	}
}])
